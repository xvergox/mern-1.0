import {useState, useCallback, useEffect} from 'react'

const storageName = 'userData'

export const useAuth = () => {
  const [token, setToken] = useState(null)    // 
  const [ready, setReady] = useState(false)   //
  const [userId, setUserId] = useState(null)  //

  const login = useCallback((jwtToken, id) => {  //заходим
    setToken(jwtToken)
    setUserId(id)

    localStorage.setItem(storageName, JSON.stringify({ //грубо говоря куки для сесии
      userId: id, token: jwtToken
    }))
  }, [])


  const logout = useCallback(() => {  //выходим
    setToken(null)
    setUserId(null)
    localStorage.removeItem(storageName)
  }, [])

  useEffect(() => {  // смотрит есть куки с такими данными(автовход)
    const data = JSON.parse(localStorage.getItem(storageName))
    if (data && data.token) {
      login(data.token, data.userId)
    }
    setReady(true)
  }, [login])


  return { login, logout, token, userId, ready }
}
