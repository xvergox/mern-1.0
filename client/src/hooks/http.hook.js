import { useState, useCallback } from 'react'// хуки из реакта + ucallbk что бы небыло рекурсии

export const useHttp = () => {  // использование afeth из браузера, позволяет взаимодействовать с сервером 
 // грузиться у нас что-то из сервера, по умолчанию нет
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(null)//

  const request = useCallback(async ( // принимает значение из браузера
    url, method = 'GET',             // принимает значение из браузера
    body = null,                    // принимает значение из браузера
    headers = {}) =>               // принимает значение из браузера
  { 
    setLoading(true)// пошла загрузка из сервера
    try {
      if (body) {
        body = JSON.stringify(body)//
        headers['Content-Type'] = 'application/json'//
      }
       //принимает из браузера
      const response = await fetch(url, { method, body, headers }) 
      console.log(`Принимает из браузера`)
      //распарсивает значения
      const data = await response.json()
      console.log(`Распарсивает значения`)
      if (!response.ok) { //есть ли ответ
        throw new Error(data.message  || ' Ошибки в !response : http.hook.js (client) ')//  
      }
      setLoading(false)
      return data   // данные из сервера
    } catch (e) {
      setLoading(false)
      setError(e.message  || ' loading : http.hook.js (client)')
      throw e
    }
  }, [])

  const clearError = useCallback(() => setError(null), [])

  return { loading, request, error, clearError }
}
