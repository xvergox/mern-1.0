import React from 'react'
import {Switch, Route, Redirect, Router} from 'react-router-dom'
import {LinksPage} from './pages/LinksPage'
import {CreatePage} from './pages/CreatePage'
import {DetailPage} from './pages/DetailPage'
import {AuthPage} from './pages/AuthPage'

export const useRoutes = isAuthenticated => {            //если зарегестрирован тогда дает вот эти ссылки
  if (isAuthenticated) {
    return (

//------------- Навбар "Список"-------------
      <Switch>
        <Route path="/links" exact>
          <LinksPage />
        </Route>


{/* -------------// Навбар "Создать"----------*/}
        <Route path="/create" exact>
          <CreatePage />
        </Route>


{/* -------------"Открыть"--------------------*/}
        <Route path="/detail/:id">
          <DetailPage />
        </Route>
        

        
        <Redirect to="/create" />
      </Switch>
    )
  }
  return (                                //если не зарагестрирован тогда бросает на страницу регистрации/авторизации 
    <Switch>
      <Route path="/" exact>
        <AuthPage />
      </Route>       
      <Redirect to="/" />
    </Switch>
  )
}
