import React, {useCallback, useContext, useEffect, useState} from 'react'
import {useHttp} from '../hooks/http.hook'
import {AuthContext} from '../context/AuthContext'
import {Loader} from '../components/Loader'

import {LinksList} from '../components/LinksList'

export const LinksPage = () => {
   //создание переменной links
   //
  const [links, setLinks] = useState([]) 
   //для определение пользователя
  const {loading, request} = useHttp()   
   //Для опреджеление состояния авторизациии
  const {token} = useContext(AuthContext) 

  const fetchLinks = useCallback(async () => {
    try {
      const fetched = await request('/api/link', 'GET', null, {
        Authorization: `Bearer ${token}`  //Доступ к созданым линкам
      })
      setLinks(fetched)
    } catch (e) {}
  }, [token, request])
  
  useEffect(() => {

    fetchLinks()

  }, [fetchLinks])
  if (loading) {
    return <Loader/>
  }
  return (
    <>
      {!loading && <LinksList links={links} />}
    </>
  )
}
export default LinksPage