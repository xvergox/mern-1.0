const {Schema, model, Types} = require('mongoose')


//регистрация

const schema = new Schema({
  email: {type: String, required: true, unique: true}, // unique: true}, - это уникальный 
  password: {type: String, required: true},

  links: [{ type: Types.ObjectId, ref: 'Link' }]   //каждый пользователь видит только свои ссылки
})

module.exports = model('User', schema)
