const {Router} = require('express')
const bcrypt = require('bcryptjs')
const config = require('config')
const jwt = require('jsonwebtoken')
const {check, validationResult} = require('express-validator')
const User = require('../models/User')
const router = Router()

//регистрация
// /api/auth/register 
router.post(
  '/register',
  [
    check('email', 'Некорректный email').isEmail(),  //проверка пормальный ли email
    check('password', 'Минимальная длина пароля 6 символов')  // проверка пароля минимально количество символов в пароле
      .isLength({ min: 6 })
  ],
  async (req, res) => {
  try {
    console.log("Body: ", req.body)
    const errors = validationResult(req) // проверка валидатора
    if (!errors.isEmpty()) {     //если поле ошибки валидатора не пустое
      return res.status(400).json({
        errors: errors.array(),
        message: 'Некорректный данные при регистрации'
      })
    }
    const {email, password} = req.body  //принимает значения с фронта -user.js
    console.log(`Принимает значения с фронта: auth.routers.js`)
    const candidate = await User.findOne({ email })  //смотрит есть такой пользователь
    if (candidate) {

      return res.status(400).json({ message: 'Такой пользователь уже существует'})

    

    }

    const hashedPassword = await bcrypt.hash(password, 12)   //шифрование пароля
    console.log(`шифрование пароля`)
    const user = new User({ email, password: hashedPassword }) //создаем пользователя
    console.log(`создаем пользователя`)
    await user.save() //ожидание сохранение пользователя
    res.status(201).json({ message: 'Пользователь создан', message: 'Ведите данные снова для входа' })
    
    console.log(`Пользователь создан : auth.routers.js`)
  } catch (e) {
    res.status(500).json({ message: 'Что-то пошло не так на этапе регистрации, попробуйте снова' })
  }
})
 //Авторизация
// /api/auth/login
router.post(
  '/login',
  [
    check('email', 'Введите корректный email').normalizeEmail().isEmail(),
    check('password', 'Введите пароль').exists() //пароль долен существовать exist()
  ],
  async (req, res) => {
  try {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.array(),
        message: 'Некорректный данные при входе в систему'   // 'Некорректный данные при входе в систему'
      })
    }
    const {email, password} = req.body  // получение из фронтента 
    const user = await User.findOne({ email })   // проверяет есть ли такой пользователь в системе
    if (!user) {
      return res.status(400).json({ message: 'Пользователь не найден' }) //'Пользователь не найден'
    }
    const isMatch = await bcrypt.compare(password, user.password) //проверяет совпадают ли пароли
    if (!isMatch) {
      return res.status(400).json({ message: 'Неверный пароль, попробуйте снова' }) // 'Неверный пароль, попробуйте снова'
    }
    const token = jwt.sign(  //создание токена 
      { userId: user.id },   
      config.get('jwtSecret'),  //серкретный ключ генирации
      { expiresIn: '4h' }   // количество времени сесии
      
    )
    // return 
    // res.status(777).json({ message: 'Добро пожаловать' }) 
    res.json({ token, userId: user.id })
  } catch (e) {
    res.status(500).json({ message: 'Что-то пошло не так на этапе авторизации, попробуйте снова' })
  }
})
//авторизация,регстрация и сесия завершена
module.exports = router
