const express = require("express");
const bodyParser = require("body-parser");
const puppeteer = require('puppeteer');
const http = require("http");
const fs = require("fs");

const mongoose = require('mongoose');


const Name = require("./app");
// const { url } = require("node:inspector");


let amazon_pars = async () => {

    const browser = await puppeteer.launch({ headless: true, });
    const page = await browser.newPage(); //open new page on the brauzer

    let url = Name;
    url = url.Name;

    //Вырезает из строки
    url = url.replace(/{}/g, '');
    url = url.replace(/Name/g, '');
    url = url.replace(/ /g, '');

    let url_amazon = url;

    page.setRequestInterception(true)
    page.on('request', async request => {
        if (
            request.resourceType() === 'media' ||
            request.resourceType() === 'teg' ||
            request.resourceType() === 'video' ||
            request.resourceType() === 'link' ||
            request.resourceType() === 'photo' ||
            request.resourceType() === 'text' ||
            request.resourceType() === 'url' ||
            request.resourceType() === 'role' ||
            request.resourceType() === 'stylesheet' ||   //6-7 sek
            request.resourceType() === 'image' ||
            request.resourceType() === 'png'
        ) {
            request.abort()
        } else {
            request.continue()
        }
    })
    /////////////////////////////////////////////////////////////amazon
    const cookies = [
        {
            "domain": ".amazon.com",
            "expirationDate": 1647366022.855767,
            "hostOnly": false,
            "httpOnly": true,
            "name": "at-main",
            "path": "/",
            "sameSite": "unspecified",
            "secure": true,
            "session": false,
            "storeId": "0",
            "value": "Atza|IwEBIBPvlo2QG-FRx_g-GESiCg0NayVODSwdEVlErzMntpcRncBeJCy8fuVth580K0LBBCZ-Hdoj27KoWKgTHMlcXYz3nN0ha0QS26YSFSPvRtdTX5RCP-Khsa3sfH_CHExjFW83stF3HZKvHQWqGTexezaOpiqs9QvvdT3ekXTmOvQpRy6v7ju00fIrkNpGg16YhCqdRSA_7t_yJV2DNH7fO0os",
            "id": 1
        },
        {
            "domain": ".amazon.com",
            "expirationDate": 1647366023.078411,
            "hostOnly": false,
            "httpOnly": false,
            "name": "i18n-prefs",
            "path": "/",
            "sameSite": "unspecified",
            "secure": false,
            "session": false,
            "storeId": "0",
            "value": "USD",
            "isProtected": true,
            "id": 2
        },
        {
            "domain": ".amazon.com",
            "expirationDate": 1647366022.855807,
            "hostOnly": false,
            "httpOnly": false,
            "name": "lc-main",
            "path": "/",
            "sameSite": "unspecified",
            "secure": true,
            "session": false,
            "storeId": "0",
            "value": "en_US",
            "id": 3
        },
        {
            "domain": ".amazon.com",
            "expirationDate": 1647366022.855781,
            "hostOnly": false,
            "httpOnly": true,
            "name": "sess-at-main",
            "path": "/",
            "sameSite": "unspecified",
            "secure": true,
            "session": false,
            "storeId": "0",
            "value": "\"KVaT3YWGaHAImFaihn+N9Mvg1vassFXmnQDyAY0D6YY=\"",
            "id": 4
        },
        {
            "domain": ".amazon.com",
            "expirationDate": 1647366027.50568,
            "hostOnly": false,
            "httpOnly": false,
            "name": "session-id",
            "path": "/",
            "sameSite": "unspecified",
            "secure": true,
            "session": false,
            "storeId": "0",
            "value": "145-9171607-7076668",
            "isProtected": true,
            "id": 5
        },
        {
            "domain": ".amazon.com",
            "expirationDate": 1647366027.505663,
            "hostOnly": false,
            "httpOnly": false,
            "name": "session-id-time",
            "path": "/",
            "sameSite": "unspecified",
            "secure": false,
            "session": false,
            "storeId": "0",
            "value": "2082787201l",
            "isProtected": true,
            "id": 6
        },
        {
            "domain": ".amazon.com",
            "expirationDate": 1647366023.078379,
            "hostOnly": false,
            "httpOnly": false,
            "name": "session-token",
            "path": "/",
            "sameSite": "unspecified",
            "secure": true,
            "session": false,
            "storeId": "0",
            "value": "\"Xl3z1Nstk5BwP5276lq67Ie6puBdKhCJ2MuYqWHkxISbeUwKXP4GahHbpFloSP6Z+3IRhRb7/Dt41opo+wxaFQq0wRPhTtSv/e3AFggdAQ+CdyZOZkWstPLx8nwb/AAT+7DuKqzKSt02vKqZNlap+yWY3xytQVBb6lLyr2+UwNAiya8cSVQq2SsyPqIHxK4twcOA5ZDpA49NqRdcqj0eQrkRX41qDy0wlURRMqPzxZ4=\"",
            "isProtected": true,
            "id": 7
        },
        {
            "domain": ".amazon.com",
            "hostOnly": false,
            "httpOnly": false,
            "name": "skin",
            "path": "/",
            "sameSite": "unspecified",
            "secure": false,
            "session": true,
            "storeId": "0",
            "value": "noskin",
            "id": 8
        },
        {
            "domain": ".amazon.com",
            "expirationDate": 1647366022.855792,
            "hostOnly": false,
            "httpOnly": true,
            "name": "sst-main",
            "path": "/",
            "sameSite": "unspecified",
            "secure": true,
            "session": false,
            "storeId": "0",
            "value": "Sst1|PQH0s7cZ_YDJL2UASVsnOTHECdif3AZvA-ixnP7h0Wl_9ZK95_zDAGN1yXo9CvXRc3OEGyPDxpnTWu3pbwMN5PXTrEspZCexlu_qVhaf8i_RuNIAJNUU3A99qVV4DF8CNW3Ydf7ihEac4REtlPNo5FAVy0tlKK6oI1zY-E3aMjIgpP66ri89QWJNGJ-GFl943NqehlGSOD7dbx7S_N_UYKEIQ7MzolfIoe5T_UbyfIoUGo8Kf5eUNnAWHcTGsf91qdrh_h8-W0ygySpbgWre_YRSVC6AKbaNqTYyaJROQvdo7EY",
            "id": 9
        },
        {
            "domain": ".amazon.com",
            "expirationDate": 1647366027.505626,
            "hostOnly": false,
            "httpOnly": false,
            "name": "ubid-main",
            "path": "/",
            "sameSite": "unspecified",
            "secure": true,
            "session": false,
            "storeId": "0",
            "value": "130-8865480-8565666",
            "isProtected": true,
            "id": 10
        },
        {
            "domain": ".amazon.com",
            "expirationDate": 1647366023.078396,
            "hostOnly": false,
            "httpOnly": false,
            "name": "x-main",
            "path": "/",
            "sameSite": "unspecified",
            "secure": true,
            "session": false,
            "storeId": "0",
            "value": "\"fYBurgQQeYarlw7A?480h86EhwWxh2jXNguwNly2qY0X0OyzBsEBi6JSa786QkMg\"",
            "id": 11
        },
        {
            "domain": "www.amazon.com",
            "expirationDate": 1645981313,
            "hostOnly": true,
            "httpOnly": false,
            "name": "csm-hit",
            "path": "/",
            "sameSite": "unspecified",
            "secure": false,
            "session": false,
            "storeId": "0",
            "value": "tb:s-JSGW01YW4DVDEJDTKVDK|1615741312361&t:1615741313587&adb:adblk_yes",
            "isProtected": true,
            "id": 12
        }
    ]


    await page.setCookie(...cookies);
    const cookiesSet = await page.cookies(url_amazon);

    await page.goto(url_amazon);  // go to link

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    await page.waitForTimeout(200);
    const result = await page.evaluate(() => {    //!!!!!
        let FB = document.querySelector('#tabular-buybox-truncate-0 > span.a-truncate-cut > span').innerText;
        let price = document.querySelector('#priceblock_ourprice').innerText;
        let seller = document.querySelector('#olp_feature_div > div.a-section.a-spacing-small.a-spacing-top-small > span > a > span:nth-child(1)').innerText;
        let BSR = document.querySelector('#acrCustomerReviewText').innerText;
        let category = document.querySelector('#wayfinding-breadcrumbs_feature_div > ul > li:nth-child(1) > span > a').innerText;
        let subcategory = document.querySelector('#wayfinding-breadcrumbs_feature_div > ul > li:nth-child(7) > span > a').innerText;
        let buybox_now = document.querySelector('#tabular-buybox-truncate-1 > span.a-truncate-cut > span').innerText;
        let brand_name = document.querySelector('#bylineInfo').innerText;
        return {
            FB,
            price,
            seller,
            BSR,
            category,
            subcategory,
            buybox_now,
            brand_name
        }
    });
    browser.close();
    return result;
};

//and there we create some varible 
amazon_pars().then((value) => {

    // console.log('FB');
    // console.log(value.FB);
    let FB_a = value.FB;
    console.log('Данные на АМАЗОН ');
    // console.log(value.price);
    let price_a = value.price;
    // console.log('Количество продавцов****************************************');
    // console.log(value.seller);
    let seller = value.seller;
    // console.log('Ранг');
    // console.log(value.BSR);
    let bsr = value.BSR;
    // console.log('Категория');
    // console.log(value.category);
    let category = value.category;
    // console.log('Подкатегория');
    // console.log(value.subcategory);
    let subcategory = value.subcategory;
    // console.log('Кнопка купить');
    // console.log(value.buybox_now);
    let buybox_now = value.buybox_now;
    // console.log('Имя брэнда');
    // console.log(value.brand_name);
    let brand_name = value.brand_name;

    // console.log('Все значения');
    // let mass_a = value;
    // console.log(mass_a);

    //********************************************************************************* */



    mongoose.Promise = global.Promise
    // console.log("*******************************************************************************************************************************************************************************************************************************************");

    console.log("Запись из Amazon в базу");

    mongoose.connect('mongodb://localhost:27017/Asin',
        { useNewUrlParser: true, useUnifiedTopology: true })
        .then(() => console.log('Запуск базы на парсере AMAZON успешен'))
        .catch(e => console.log(e))
    require('./amazon.model')


    // const Name = require("./app");
    // const { url } = require("node:inspector");

    // let ur = Name;
    // ur = ur.Name;
    const Amazon = mongoose.model('AmazonData')
    const person = new Amazon({

        // Name_U: ur,

        FB_a: FB_a,
        price_a: price_a,
        seller: seller,
        bsr: bsr,
        category: category,
        subcategorye: subcategory,
        buybox_now: buybox_now,
        brand_name: brand_name,
    })
    person.save()
        .then(user => {
            // console.log(user);
            console.log("В базу результат из Амазон записан");
        })
    // .catch(e => console.log(e)); //вывод в логи из базы




    // console.log("***************************************************************");

    //нужно передать информацию в базу

});

module.exports = amazon_pars;






